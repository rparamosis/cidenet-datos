-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3308
-- Tiempo de generación: 10-06-2022 a las 19:46:23
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crud`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `primerNombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundoNombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primerApellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundoApellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fechaIngreso` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `estado` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `primerNombre`, `segundoNombre`, `primerApellido`, `segundoApellido`, `pais`, `ti`, `documento`, `email`, `area`, `fechaIngreso`, `estado`, `created_at`, `updated_at`) VALUES
(32, 'Ricardo', 'Adolfo', 'Paramo', 'Lopez', 'Estados Unidos', 'Cédula de Ciudadanía', '9865751', 'Ricardo.Paramo@ccidenet.com.us', 'Administración', '2022-06-09 00:00:00', 'A', '2022-06-10 22:41:27', '2022-06-10 22:41:27'),
(33, 'Marial', 'Nelly', 'Lopez', '-', 'Colombia', 'Cédula de Ciudadanía', '5422136', 'Marial.Lopez@ccidenet.com.us', 'Administración', '2022-06-10 00:00:00', 'A', '2022-06-10 22:45:49', '2022-06-10 22:45:49');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuarios_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
