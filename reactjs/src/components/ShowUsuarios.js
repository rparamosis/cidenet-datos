import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";



const ruta = "http://localhost:8000/api";

export const ShowUsuarios = () => {
  const [usuarios, setUsuarios] = useState( [] );

  useEffect(() => {
    getUsuarios();
  }, []);

  const getUsuarios = async () => {
    const response = await axios.get(`${ruta}/usuarios`);
    setUsuarios(response.data);
  };
  

  const borrarUsuarios = async (id) => {

    if(window.confirm(" Desea eliminar este usuario ?")){
      axios.delete(`${ruta}/usuario/${id}`); 
      getUsuarios();
    }

  };

  return (
    
    <div className=" ingresar">
       <div className="contenido-principal contenido">
       <div className="d-grid gap-2 ingresar" >
        <Link
          to="/create"
          className="btn btn-info btn-lg mt-2 mb text-white ">
         INGRESAR NUEVO USUARIO
        </Link>
      </div>


       
      <table className="table table table-striped">
        <thead >
            <tr>  
          <th colspan="2">NOMBRES</th>        
          <th colspan="2">APELLIDOS</th>
          <th>PAIS DE TRABAJO</th>
          <th>IDENTIFICACION</th>
          <th>DOCUMENTO</th>
          <th>EMAIL</th>
          <th>AREA</th>
          <th>FECHA_INGRESO</th>
          </tr>
        </thead>
        <tbody>
          { usuarios.map((usuario) => (
            <tr key={usuario.id}>
              <td>{usuario.primerNombre}</td>
              <td>{usuario.segundoNombre}</td>
              <td>{usuario.primerApellido}</td>
              <td>{usuario.segundoApellido}</td>
              <td>{usuario.pais}</td>
              <td>{usuario.ti}</td>
              <td>{usuario.documento}</td>
              <td>{usuario.email}</td>
              <td>{usuario.area}</td>
              <td>{usuario.fechaIngreso}</td>
              <td>
                <Link to={`/edit/${usuario.id}`} className="btn btn-info">
                  Editar
                </Link>  
                &nbsp;&nbsp; 
                <button onClick={() => borrarUsuarios(usuario.id)} className="btn btn-danger">
                  Borrar
                </button>
               
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
  );
};

export default ShowUsuarios;
