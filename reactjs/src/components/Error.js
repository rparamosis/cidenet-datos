import React from "react";
import Prototype from 'prop-types';

const Error = ({mensaje}) => {

 return (
    <p className="mensaje"> {mensaje}   </p> 

 )   
}

Error.Prototypes ={
    mensaje: Prototype.string.isRequired
}

export default Error;