import React, { useState } from "react";
import axios from "axios";
import { useNavigate, Link } from "react-router-dom";
import Error from "./Error";

const ruta = "http://localhost:8000/api/usuario";

export const CreateUsuarios = () => {


   
  const [primerNombre, setPrimerNombre] = useState("");
  const [segundoNombre, setSegundoNombre] = useState("");
  const [primerApellido, setPrimerApellido] = useState("");
  const [segundoApellido, setSegundoApellido] = useState("");
  const [pais, setPais] = useState("");
  const [ti, setTi] = useState("");
  const [documento, setdocumento] = useState("");
  const [area, setArea] = useState("");
  const [fechaIngreso, setFehaIngreso] = useState("");
  const [error, guardarError] = useState(false);
  const [errorT, guardarErrorT] = useState(false);
  const [errorf, guardarErrorF] = useState(false);

  const navegacion = useNavigate();
  const permitidos = /[^ A-Za-z0-9--]/;
  const tamano = (e) => {
    const entrada = e.target.value;
    

    if (permitidos.test(entrada)) {
      guardarErrorT(true);
      return false;
    } else {
      guardarErrorT(false);
    }
  };

  const datos = async (e) => {
    e.preventDefault();

    if (
      documento.trim() === "" ||
      primerNombre.trim() === "" ||
      primerApellido.trim() === "" ||
      pais.trim() === "" ||
      ti === "0" ||  ti.trim()==="" ||
      area === "0" ||  area.trim()===""
    ) {
      guardarError(true);
      guardarErrorT(false);
      return false;
    }

    if (permitidos.test(documento)) {
      guardarErrorF(true);
      guardarErrorT(false);
      return false;
    }
    guardarError(false);

    await axios
      .post(ruta, {
        primerNombre: primerNombre,
        segundoNombre: segundoNombre,
        primerApellido: primerApellido,
        segundoApellido: segundoApellido,
        pais: pais,
        ti: ti,
        documento: documento,
        area:area,
        fechaIngreso:fechaIngreso,
      })
      .then(function (response) {
        navegacion("/");
      })
      .catch(function (error) {
        if (error.response) {
          alert(error.response.data);
        }
      });
  };

  return (
    <div className="container ingresar">
      <div className="contenido-principal contenido">
        <h3 className="ingresar">Crear Nuevo Usuario</h3>

        <form onSubmit={datos}>
          {error ? <Error mensaje="Los campos no pueden estar vacíos" /> : null}
          {errorT ? (<Error mensaje="No debe colocar caracteres especiales" />) : null}
          {errorf ? (<Error mensaje="No puede grabar caracteres especiales" />) : null}
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Documento *
            </label>
            <div className="col-sm-10">
              <input
                value={documento}
                onChange={(e) => setdocumento(e.target.value)}
                type="text"
                className="form-control"
                onKeyUp={tamano}
                maxLength="20"
              />
            </div>
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Primer Nombre *
            </label>
            <div className="col-sm-10">
              <input
                value={primerNombre}
                onChange={(e) => setPrimerNombre(e.target.value)}
                type="text"
                className="form-control"
                onKeyUp={tamano}
                maxLength="20"
              />
            </div>
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Segundo Nombre
            </label>
            <div className="col-sm-10">
              <input
                value={segundoNombre}
                onChange={(e) => setSegundoNombre(e.target.value)}
                type="text"
                className="form-control"
                onKeyUp={tamano}
                maxLength="50"
              />
            </div>
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Primer Apellido *
            </label>
            <div className="col-sm-10">
              <input
                value={primerApellido}
                onChange={(e) => setPrimerApellido(e.target.value)}
                type="text"
                className="form-control"
                onKeyUp={tamano}
                maxLength="20"
              />
            </div>
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Segundo Apellido
            </label>
            <div className="col-sm-10">
              <input
                value={segundoApellido}
                onChange={(e) => setSegundoApellido(e.target.value)}
                type="text"
                className="form-control"
                onKeyUp={tamano}
                maxLength="50"
              />
            </div>
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">Pais *</label>
            <div className="col-sm-5">
              <input
                type="radio"
                value="Colombia"
                name="documento"
                onChange={(e) => setPais(e.target.value)}
                className=""
              />
              &nbsp;&nbsp; Colombia
            </div>
            <div className="col-sm-5">
              <input
                type="radio"
                value="Estados Unidos"
                name="documento"
                onChange={(e) => setPais(e.target.value)}
                className=""
              />
              &nbsp;&nbsp; Estados Unidos
            </div>
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Tipo de Identificación *
            </label>
            <div className="col-sm-10">
              <select
                value={ti}
                onChange={(e) => setTi(e.target.value)}
                type="text"
                className="form-control"
              >
                <option value="0">
                  Escoger Tipo
                </option>
                <option value="Cédula de Ciudadanía">Cédula de Ciudadanía</option>
                <option value="Cédula de Extranjería">Cédula de Extranjería</option>
                <option value="Pasaporte">Pasaporte</option>
                <option value="Permiso Especial">Permiso Especial</option>
              </select>
            </div>
          </div>
          <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Área *
            </label>
          <div className="col-sm-10">
              <select
                value={area}
                onChange={(e) => setArea(e.target.value)}
                type="text"
                className="form-control"
              >
                <option value="0">
                  Escoger Área
                </option>
                <option value="Administración">Administración</option>
                <option value="Financiera">Financiera</option>
                <option value="Compras">Compras</option>
                <option value="Infraestructura">Infraestructura</option>
                <option value="Operación">Operación</option>
                <option value="Talento Humano">Talento Humano</option>
                <option value="Servicios">Servicios</option>
              </select>
            </div>          
            </div>          
            <div className="mb-3 row">
            <label className="col-sm-2 col-form-label labelText">
              Fecha de Ingreso
            </label>
            <div className="col-sm-10">
              <input
                value={fechaIngreso}
                onChange={(e) => setFehaIngreso(e.target.value)}
                type="date"
                className="form-control"
                onKeyUp={tamano}
                maxLength="50"
              />
            </div>
          </div>

          <button type="submit" className="btn btn-primary">
            Guardar
          </button>
          &nbsp;&nbsp; &nbsp;&nbsp;
          <Link to={"/"}>
            <button type="submit" className="btn btn-success  ">
              Regresar
            </button>
          </Link>
        </form>
      </div>
    </div>
  );
};

export default CreateUsuarios;
