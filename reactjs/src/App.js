import './App.css';
import ShowUsuarios from './components/ShowUsuarios';
import CreateUsuarios from './components/CreateUsuarios';
import EditarUsuarios from './components/EditarUsuarios';
import logo from './assets/img/cidenet-software-a-la-medida-medellin.png'

import { BrowserRouter, Routes,Route} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <img src={logo} className="logo"/>
    <BrowserRouter>
    <Routes>
      <Route path='/'  element={<ShowUsuarios/>}  ></Route>
      <Route path='/create'  element={<CreateUsuarios/>}  ></Route>
      <Route path='/edit/:id'  element={<EditarUsuarios/>}  ></Route>
    </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
