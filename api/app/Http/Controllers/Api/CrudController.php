<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Usuario;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios=Usuario::all();
        return $usuarios;

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fechaActual=date('Ymd');
        $fechaAnterior=$fechaActual-30;
        $fecha=$request->fechaIngreso;

        $quitar= str_replace("-","",$fecha);   

        if($quitar > $fechaActual ){
        return response()->json("la fecha no puede ser mayor a la actual", 400);
           
        }
        if($quitar < $fechaAnterior ){
        return response()->json("Seleccione un fecha o la fecha no puede ser menor a un mes", 400);
           
        }

        $tipo=$request->ti;
        if($tipo==1){
            $mail="@cidenet.com.co";
        }else{
            $mail="@ccidenet.com.us";
        }

        $segN= $request->segundoNombre;
        if($segN==""){
            $request->segundoNombre="-";
        }
        $segA= $request->segundoApellido;
        if($segA==""){
            $request->segundoApellido="-";
        }
    

        $email= $request->primerNombre.".".$request->primerApellido.$mail;
        
        $cantidad = Usuario::where([['primerNombre', '=', $request->primerNombre],['primerApellido', '=',$request->primerApellido]])->count();        
       
        if($cantidad > 0){         
            $id=$cantidad;
            $email= $request->primerNombre.".".$request->primerApellido.".".$id.$mail;
            // return response()->json("El correo ya existe", 400);
        }

        $usuario= new Usuario();
        $usuario->primerNombre = $request->primerNombre;
        $usuario->segundoNombre = $request->segundoNombre;
        $usuario->primerApellido = $request->primerApellido;
        $usuario->segundoApellido = $request->segundoApellido;
        $usuario->pais = $request->pais;
        $usuario->ti = $request->ti;
        $usuario->documento = $request->documento;
        $usuario->email  = $email ;
        $usuario->area  =  $request->area;
        $usuario->fechaIngreso  =  $request->fechaIngreso;
        $usuario->save();

        return response()->json($usuario,200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario= Usuario::find($id);
        return $usuario;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $segN= $request->segundoNombre;
        if($segN==""){
            $request->segundoNombre="-";
        }
        $segA= $request->segundoApellido;
        if($segA==""){
            $request->segundoApellido="-";
        }

        $usuario= Usuario::findOrFail($request->id);
        $usuario->primerNombre = $request->primerNombre;
        $usuario->segundoNombre = $request->segundoNombre;
        $usuario->primerApellido = $request->primerApellido;
        $usuario->segundoApellido = $request->segundoApellido;
        $usuario->pais = $request->pais;
        $usuario->ti = $request->ti;
        $usuario->documento = $request->documento;
        $usuario->email  = $request->email ;
        $usuario->area  =  $request->area;
        $usuario->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::destroy($id);
        return $usuario;
    }
}
